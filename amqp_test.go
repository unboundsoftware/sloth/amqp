/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package amqp

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/sanity-io/litter"
	"github.com/sparetimecoders/goamqp"
	"github.com/stretchr/testify/assert"

	"gitlab.com/unboundsoftware/sloth/model"
)

func TestSink_Start(t *testing.T) {
	type fields struct {
		connection Connection
	}
	type args struct {
		in0 chan error
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "error starting",
			fields: fields{
				connection: &MockConnection{
					start: func(ctx context.Context, opts ...goamqp.Setup) error {
						if len(opts) != 2 {
							t.Errorf("Start() got %d opts, want 2", len(opts))
						}
						return errors.New("error")
					},
				},
			},
			args:    args{},
			wantErr: true,
		},
		{
			name: "success",
			fields: fields{
				connection: &MockConnection{
					start: func(ctx context.Context, opts ...goamqp.Setup) error {
						return nil
					},
				},
			},
			args:    args{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s := &Sink{
				connection: tt.fields.connection,
				Logger:     logger,
			}
			if err := s.Start(model.ChannelQuitter(tt.args.in0)); (err != nil) != tt.wantErr {
				t.Errorf("Start() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSink_Publish(t *testing.T) {
	type fields struct {
		connection Connection
	}
	type args struct {
		request model.Request
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "error publishing",
			fields: fields{
				connection: &MockConnection{
					publish: func(ctx context.Context, targetService, routingKey string, msg interface{}) error {
						wantService := "host"
						wantKey := "path"
						wantMsg := json.RawMessage("abc")
						if targetService != wantService {
							t.Errorf("Publish() got %s, want %s", targetService, wantService)
						}
						if routingKey != wantKey {
							t.Errorf("Publish() got %s, want %s", routingKey, wantKey)
						}
						if !reflect.DeepEqual(msg, wantMsg) {
							t.Errorf("Publish() got %s, want %s", litter.Sdump(msg), litter.Sdump(wantMsg))
						}
						return errors.New("error")
					},
				},
			},
			args: args{
				request: model.Request{
					Target:  "test://host/path",
					Payload: []byte("abc"),
				},
			},
			wantErr: true,
		},
		{
			name: "success",
			fields: fields{
				connection: &MockConnection{
					publish: func(ctx context.Context, targetService, routingKey string, msg interface{}) error {
						return nil
					},
				},
			},
			args: args{
				request: model.Request{
					Target:  "test://host/path",
					Payload: []byte("abc"),
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Sink{
				connection: tt.fields.connection,
			}
			if err := s.Publish(tt.args.request); (err != nil) != tt.wantErr {
				t.Errorf("Publish() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSink_Stop(t *testing.T) {
	type fields struct {
		connection Connection
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "error closing",
			fields: fields{
				connection: &MockConnection{
					close: func() error {
						return errors.New("error")
					},
				},
			},
			wantErr: true,
		},
		{
			name: "success",
			fields: fields{
				connection: &MockConnection{
					close: func() error {
						return nil
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s := &Sink{
				connection: tt.fields.connection,
				Logger:     logger,
			}
			if err := s.Stop(); (err != nil) != tt.wantErr {
				t.Errorf("Stop() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSink_Accept(t *testing.T) {
	type args struct {
		scheme string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "amqp",
			args: args{scheme: "amqp"},
			want: true,
		},
		{
			name: "other",
			args: args{scheme: "other"},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Sink{}
			if got := s.Accept(tt.args.scheme); got != tt.want {
				t.Errorf("Accept() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSource_Start(t *testing.T) {
	type fields struct {
		connection Connection
	}
	type args struct {
		handler model.Storer
		in1     chan error
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "error starting",
			fields: fields{
				connection: &MockConnection{start: func(ctx context.Context, opts ...goamqp.Setup) error {
					if len(opts) != 4 {
						t.Errorf("Start() got %d, want 4", len(opts))
					}
					return errors.New("error")
				}},
			},
			args:    args{},
			wantErr: true,
		},
		{
			name: "success",
			fields: fields{
				connection: &MockConnection{start: func(ctx context.Context, opts ...goamqp.Setup) error {
					return nil
				}},
			},
			args:    args{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s := &Source{
				connection: tt.fields.connection,
				Logger:     logger,
			}
			if err := s.Start(tt.args.handler, model.ChannelQuitter(tt.args.in1)); (err != nil) != tt.wantErr {
				t.Errorf("Start() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSource_Stop(t *testing.T) {
	type fields struct {
		connection Connection
	}
	tests := []struct {
		name       string
		fields     fields
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "error stopping",
			fields: fields{
				connection: &MockConnection{close: func() error {
					return errors.New("error")
				}},
			},
			wantErr:    true,
			wantLogged: []string{"[INFO]  stopping\n"},
		},
		{
			name: "success",
			fields: fields{
				connection: &MockConnection{close: func() error {
					return nil
				}},
			},
			wantErr:    false,
			wantLogged: []string{"[INFO]  stopping\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s := &Source{
				connection: tt.fields.connection,
				Logger:     logger,
			}
			if err := s.Stop(); (err != nil) != tt.wantErr {
				t.Errorf("Stop() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestSource_handle(t *testing.T) {
	type args struct {
		handler model.Storer
		msg     interface{}
		headers goamqp.Headers
	}
	tests := []struct {
		name       string
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name:    "missing delay until header",
			args:    args{},
			wantErr: true,
		},
		{
			name: "missing target header",
			args: args{
				headers: goamqp.Headers{
					"delay-until": time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC),
				},
			},
			wantErr: true,
		},
		{
			name: "invalid URL target header",
			args: args{
				headers: goamqp.Headers{
					"delay-until": time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC),
					"target":      fmt.Sprintf("%c", 0x1f),
				},
			},
			wantErr: true,
		},
		{
			name: "error handling",
			args: args{
				handler: &MockHandler{
					handle: func(request model.Request) error {
						want := model.Request{
							DelayedUntil: time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC),
							Target:       "amqp://host/path",
							Payload:      json.RawMessage("abc"),
						}
						if !reflect.DeepEqual(request, want) {
							t.Errorf("handle() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
						}
						return errors.New("error")
					},
				},
				msg: ptr(json.RawMessage("abc")),
				headers: goamqp.Headers{
					"delay-until": time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC),
					"target":      "amqp://host/path",
				},
			},
			wantErr: true,
		},
		{
			name: "delay until as int",
			args: args{
				handler: &MockHandler{
					handle: func(request model.Request) error {
						want := model.Request{
							DelayedUntil: time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC),
							Target:       "amqp://host/path",
							Payload:      json.RawMessage("abc"),
						}
						if !reflect.DeepEqual(request, want) {
							t.Errorf("handle() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
						}
						return nil
					},
				},
				msg: ptr(json.RawMessage("abc")),
				headers: goamqp.Headers{
					"delay-until": int(time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC).Unix()),
					"target":      "amqp://host/path",
				},
			},
			wantErr: false,
		},
		{
			name: "delay until as int32",
			args: args{
				handler: &MockHandler{
					handle: func(request model.Request) error {
						want := model.Request{
							DelayedUntil: time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC),
							Target:       "amqp://host/path",
							Payload:      json.RawMessage("abc"),
						}
						if !reflect.DeepEqual(request, want) {
							t.Errorf("handle() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
						}
						return nil
					},
				},
				msg: ptr(json.RawMessage("abc")),
				headers: goamqp.Headers{
					"delay-until": int32(time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC).Unix()),
					"target":      "amqp://host/path",
				},
			},
			wantErr: false,
		},
		{
			name: "delay until as int64",
			args: args{
				handler: &MockHandler{
					handle: func(request model.Request) error {
						want := model.Request{
							DelayedUntil: time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC),
							Target:       "amqp://host/path",
							Payload:      json.RawMessage("abc"),
						}
						if !reflect.DeepEqual(request, want) {
							t.Errorf("handle() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
						}
						return nil
					},
				},
				msg: ptr(json.RawMessage("abc")),
				headers: goamqp.Headers{
					"delay-until": time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC).Unix(),
					"target":      "amqp://host/path",
				},
			},
			wantErr: false,
		},
		{
			name: "delay until as string",
			args: args{
				handler: &MockHandler{
					handle: func(request model.Request) error {
						want := model.Request{
							DelayedUntil: time.Date(2021, 1, 25, 12, 0, 0, 0, time.UTC),
							Target:       "amqp://host/path",
							Payload:      json.RawMessage("abc"),
						}
						if !reflect.DeepEqual(request, want) {
							t.Errorf("handle() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
						}
						return nil
					},
				},
				msg: ptr(json.RawMessage("abc")),
				headers: goamqp.Headers{
					"delay-until": "strange value",
					"target":      "amqp://host/path",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s := &Source{
				Logger: logger,
			}
			if _, err := s.handle(tt.args.handler)(tt.args.msg, tt.args.headers); err != nil != tt.wantErr {
				t.Errorf("handle() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func ptr(m json.RawMessage) *json.RawMessage {
	return &m
}

type MockConnection struct {
	start   func(ctx context.Context, opts ...goamqp.Setup) error
	publish func(ctx context.Context, targetService, routingKey string, msg interface{}) error
	close   func() error
}

func (m MockConnection) Start(ctx context.Context, opts ...goamqp.Setup) error {
	if m.start != nil {
		return m.start(ctx, opts...)
	}
	return nil
}

func (m MockConnection) Close() error {
	if m.close != nil {
		return m.close()
	}
	return nil
}

func (m MockConnection) PublishServiceResponse(ctx context.Context, targetService, routingKey string, msg interface{}) error {
	if m.publish != nil {
		return m.publish(ctx, targetService, routingKey, msg)
	}
	return nil
}

var _ Connection = &MockConnection{}

type MockHandler struct {
	handle func(request model.Request) error
}

func (m MockHandler) Store(request model.Request) error {
	if m.handle != nil {
		return m.handle(request)
	}
	return nil
}

var _ model.Storer = &MockHandler{}

func TestSource_Configure(t *testing.T) {
	tests := []struct {
		name       string
		args       []string
		connect    func(t *testing.T) func(url string) (Connection, error)
		want       bool
		wantErr    assert.ErrorAssertionFunc
		wantLogged []string
	}{
		{
			name: "invalid parameter",
			args: []string{"--invalid"},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "unknown flag --invalid")
			},
			wantLogged: nil,
		},
		{
			name:       "no parameters",
			args:       []string{},
			want:       false,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
		{
			name: "connection failure",
			args: []string{"--amqp-url", "amqp://a:b@:5672/"},
			connect: func(t *testing.T) func(url string) (Connection, error) {
				return func(url string) (Connection, error) {
					assert.Equal(t, "amqp://a:b@:5672/", url)
					return nil, fmt.Errorf("connection error")
				}
			},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "connection error")
			},
			wantLogged: nil,
		},
		{
			name: "success",
			args: []string{"--amqp-url", "amqp://a:b@:5672/"},
			connect: func(t *testing.T) func(url string) (Connection, error) {
				return func(url string) (Connection, error) {
					return nil, nil
				}
			},
			want:       true,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s := &Source{
				Logger: logger,
			}
			if tt.connect != nil {
				connect = tt.connect(t)
			}
			got, err := s.Configure(tt.args)
			if !tt.wantErr(t, err, fmt.Sprintf("Configure(%v)", tt.args)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Configure(%v)", tt.args)
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestSource_IsSource(t *testing.T) {
	assert.Equalf(t, true, (&Source{}).IsSource(), "IsSource()")
}

func TestSource_IsSink(t *testing.T) {
	assert.Equalf(t, false, (&Source{}).IsSink(), "IsSink()")
}

func TestSource_IsStore(t *testing.T) {
	assert.Equalf(t, false, (&Source{}).IsStore(), "IsStore()")
}

func TestSink_Configure(t *testing.T) {
	tests := []struct {
		name       string
		args       []string
		connect    func(t *testing.T) func(url string) (Connection, error)
		want       bool
		wantErr    assert.ErrorAssertionFunc
		wantLogged []string
	}{
		{
			name: "invalid parameter",
			args: []string{"--invalid"},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "unknown flag --invalid")
			},
			wantLogged: nil,
		},
		{
			name:       "no parameters",
			args:       []string{},
			want:       false,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
		{
			name: "connection failure",
			args: []string{"--amqp-url", "amqp://a:b@:5672/"},
			connect: func(t *testing.T) func(url string) (Connection, error) {
				return func(url string) (Connection, error) {
					assert.Equal(t, "amqp://a:b@:5672/", url)
					return nil, fmt.Errorf("connection error")
				}
			},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "connection error")
			},
			wantLogged: nil,
		},
		{
			name: "success",
			args: []string{"--amqp-url", "amqp://a:b@:5672/"},
			connect: func(t *testing.T) func(url string) (Connection, error) {
				return func(url string) (Connection, error) {
					return nil, nil
				}
			},
			want:       true,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s := &Sink{
				Logger: logger,
			}
			if tt.connect != nil {
				connect = tt.connect(t)
			}
			got, err := s.Configure(tt.args)
			if !tt.wantErr(t, err, fmt.Sprintf("Configure(%v)", tt.args)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Configure(%v)", tt.args)
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestSink_IsSource(t *testing.T) {
	assert.Equalf(t, false, (&Sink{}).IsSource(), "IsSource()")
}

func TestSink_IsSink(t *testing.T) {
	assert.Equalf(t, true, (&Sink{}).IsSink(), "IsSink()")
}

func TestSink_IsStore(t *testing.T) {
	assert.Equalf(t, false, (&Sink{}).IsStore(), "IsStore()")
}
