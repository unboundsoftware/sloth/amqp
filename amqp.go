/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package amqp

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"time"

	"github.com/alecthomas/kong"
	"github.com/hashicorp/go-hclog"
	"github.com/sparetimecoders/goamqp"

	"gitlab.com/unboundsoftware/sloth/model"
)

type Config struct {
	AmqpUrl *url.URL `name:"amqp-url" env:"AMQP_URL" help:"Url to AMQP server to use for source and sink"`
}

type Source struct {
	connection  Connection
	Logger      hclog.Logger
	closeEvents chan error
}

func (s *Source) Configure(args []string) (bool, error) {
	cli := &Config{}
	cmd, err := kong.New(
		cli,
		kong.Description("amqp_source is a pluggable source for sloth"),
		kong.Writers(os.Stdout, os.Stdin),
	)
	if err != nil {
		return false, err
	}
	_, err = cmd.Parse(args)
	if err != nil {
		return false, err
	}

	if cli.AmqpUrl == nil {
		return false, nil
	}

	connection, err := connect(cli.AmqpUrl.String())
	if err != nil {
		return false, err
	}
	closeEvents := make(chan error, 10)
	s.connection = connection
	s.closeEvents = closeEvents
	return true, nil
}

func (s *Source) IsSource() bool {
	return true
}

func (s *Source) IsSink() bool {
	return false
}

func (s *Source) IsStore() bool {
	return false
}

func (s *Source) Start(handler model.Storer, quitter model.Quitter) error {
	go func() {
		err := <-s.closeEvents
		s.Logger.Error("received close from AMQP", "error", err)
		quitter.Quit(err)
	}()
	return s.connection.Start(
		context.Background(),
		goamqp.WithPrefetchLimit(20),
		goamqp.ServiceRequestConsumer("delay", s.handle(handler), json.RawMessage{}),
		goamqp.CloseListener(s.closeEvents),
		goamqp.UseLogger(func(str string) {
			s.Logger.Error(str)
		}),
	)
}

func (s *Source) Stop() error {
	s.Logger.Info("stopping")
	return s.connection.Close()
}

func (s *Source) handle(handler model.Storer) func(msg interface{}, headers goamqp.Headers) (response interface{}, err error) {
	return func(msg interface{}, headers goamqp.Headers) (interface{}, error) {
		temp := headers.Get("delay-until")
		if temp == nil {
			return nil, fmt.Errorf("delay-until header not present")
		}
		var delayUntil time.Time
		switch e := temp.(type) {
		case time.Time:
			delayUntil = e
		case int:
			delayUntil = time.Unix(int64(e), 0).UTC()
		case int32:
			delayUntil = time.Unix(int64(e), 0).UTC()
		case int64:
			delayUntil = time.Unix(e, 0).UTC()
		default:
			return nil, fmt.Errorf("delay-until must be set to either a time.Time value or an int representing the unix time")
		}
		strTarget, ok := headers.Get("target").(string)
		if !ok {
			return nil, fmt.Errorf("could not fetch target header")
		}
		target, err := url.Parse(strTarget)
		if err != nil {
			return nil, fmt.Errorf("could not parse target to valid URL: %s", err)
		}
		return nil, handler.Store(model.Request{
			DelayedUntil: delayUntil,
			Target:       (*target).String(),
			Payload:      *msg.(*json.RawMessage),
		})
	}
}

var _ model.Source = &Source{}

type Sink struct {
	connection  Connection
	Logger      hclog.Logger
	closeEvents chan error
}

func (s *Sink) Configure(args []string) (bool, error) {
	cli := &Config{}
	cmd, err := kong.New(
		cli,
		kong.Description("amqp_sink is a pluggable source for sloth"),
		kong.Writers(os.Stdout, os.Stdin),
	)
	if err != nil {
		return false, err
	}
	_, err = cmd.Parse(args)
	if err != nil {
		return false, err
	}

	if cli.AmqpUrl == nil {
		return false, nil
	}

	connection, err := connect(cli.AmqpUrl.String())
	if err != nil {
		return false, err
	}
	closeEvents := make(chan error, 10)
	s.connection = connection
	s.closeEvents = closeEvents
	return true, nil
}

func (s *Sink) IsSource() bool {
	return false
}

func (s *Sink) IsSink() bool {
	return true
}

func (s *Sink) IsStore() bool {
	return false
}

func (s *Sink) Start(quitter model.Quitter) error {
	go func() {
		err := <-s.closeEvents
		s.Logger.Error("received close from AMQP", "error", err)
		quitter.Quit(err)
	}()
	return s.connection.Start(
		context.Background(),
		goamqp.CloseListener(s.closeEvents),
		goamqp.UseLogger(func(str string) {
			s.Logger.Error(str)
		}),
	)
}

func (s *Sink) Publish(request model.Request) error {
	parsed, err := url.Parse(request.Target)
	if err != nil {
		return err
	}
	return s.connection.PublishServiceResponse(context.Background(), parsed.Host, parsed.Path[1:], request.Payload)
}

func (s *Sink) Stop() error {
	s.Logger.Info("stopping")
	return s.connection.Close()
}

func (s *Sink) Accept(scheme string) bool {
	return scheme == "amqp"
}

var _ model.Sink = &Sink{}

type Connection interface {
	Start(ctx context.Context, opts ...goamqp.Setup) error
	Close() error
	PublishServiceResponse(ctx context.Context, targetService, routingKey string, msg any) error
}

func connectAmqp(url string) (Connection, error) {
	return goamqp.NewFromURL("sloth", url)
}

var connect = connectAmqp
